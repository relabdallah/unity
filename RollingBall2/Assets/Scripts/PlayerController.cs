﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {

	private Rigidbody rb;
	public float speed;
	private int count;
	public Text countText;
	public Text winText;
	public Text timerText;
	private float startTime;
	private bool gameOver = false;

	void Start()
	{
		rb = GetComponent<Rigidbody> ();
		count = 0;
		SetCountText ();
		winText.text = "";
		startTime = Time.time;
	}

	void Update()
	{
		if(Input.GetKeyDown(KeyCode.R))
			ResetGame ();
		
		if (gameOver)
			return;
		
		float t = Time.time - startTime;

		string minutes = ((int)t / 60).ToString ();
		string seconds = (t % 60).ToString ("f2");

		timerText.text = "Timer: " + minutes + ":" + seconds;

	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");

		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);

		rb.AddForce (movement * speed);
	}

	void OnTriggerEnter(Collider other) 
	{
		if(other.gameObject.CompareTag("Pick Up"))
			{
			other.gameObject.SetActive (false);
			count++;
			SetCountText ();
			}
	}

	void SetCountText()
	{
		countText.text = "Count: " + count.ToString ();
		if (count >= 25) 
		{
			gameOver = true;
			winText.text = "You Win!!";
		}
	}

	void ResetGame()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		
	}

}