﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

	public Canvas MainCanvas;

	void Awake(){
		MainCanvas.enabled = true;
	}
	public void StartGame(){
		Application.LoadLevel (1);
	}

	public void Quit(){
		Application.Quit ();
	}
}
